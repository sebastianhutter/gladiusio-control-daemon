# copied from https://github.com/gladiusio/gladius-edge-docker/blob/master/Dockerfile-control
FROM node:9-alpine

# expose ports for the control daemon
EXPOSE 3000/tcp

# install dependencies and setup npm directory
RUN apk add --no-cache --update git python krb5 krb5-libs gcc make g++ krb5-dev \
 && mkdir /npm \
 && chown node:node /npm

# become user node and setup installation directory
USER node
ENV PATH=/npm/bin:$PATH
ENV NPM_CONFIG_PREFIX=/npm

# install the control daemon
RUN npm install gladius-control-daemon -g

# run gladius control daemon
CMD [ "gladius-control" ]
